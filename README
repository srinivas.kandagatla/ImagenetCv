Here are few steps required to get Imagenet based on OpenCV working DB845c

######################## On Host #################################

Step 1: Build Hexagon NN DSP library for DB845c CDSP
------------------------------------------------------
## First download (https://developer.qualcomm.com/downloads/hexagon-sdk-v342-linux?referrer=node/6116)
and Install Hexagon SDK 3.4.2 if you have not already done
## Compile Hexagon NN with below commands.

$ source $HEXAGON_SDK_ROOT/setup_sdk_env.source
$ cd $HEXAGON_SDK_ROOT/libs/hexagon_nn/2.5/
$ make tree VERBOSE=1 CDSP_FLAG=1 V=hexagon_Release_dynamic_toolv82_v65 V65=1

This step will generate hexagon_Release_dynamic_toolv82_v65/ship/libhexagon_nn_skel.so
which is a shared library for the CDSP.

Step 2: Generate Testsig and sign libhexagon_nn_skel.so with it.
------------------------------------------------------
##Get serial number from the DB845c console using below command and pass it to elfsigner.py.

root@linaro-alip:/# cat /sys/bus/soc/devices/soc0/serial_number

##elfsigner tool takes SoC Serial number as input to generate a testsig, you can get
##serial number by using getserial or from board sysfs entry as described in above step

$ cd $HEXAGON_SDK_ROOT
$ python tools/elfsigner/elfsigner.py -t $SERIAL_NUMBER
$ python tools/elfsigner/elfsigner.py -i libs/hexagon_nn/2.5/hexagon_Release_dynamic_toolv82_v65/ship/libhexagon_nn_skel.so

Step 3: Generate Inception v3 model graph for hexagon.
------------------------------------------------------

Option 1: Download the Tensorflow Inception v3 Frozen graph and covert to hexagon compatible c file.
========
$ curl http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz -o /tmp/inceptionv3.tgz
$ tar xzf /tmp/inceptionv3.tgz -C /tmp/

## Then download and Install bazel and build tensorflow More instructions at https://docs.bazel.build/versions/master/install-ubuntu.html
## I have used Bazel 0.21.0 and TensorFlow 1.13.2 release version.

$ wget https://github.com/bazelbuild/bazel/releases/download/0.21.0/bazel-0.21.0-installer-linux-x86_64.sh
$ wget https://github.com/tensorflow/tensorflow/archive/v1.13.2.tar.gz
$ chmod +x bazel-0.21.0-installer-linux-x86_64.sh
$ sh ./bazel-0.21.0-installer-linux-x86_64.sh --user
$ tar -zxvf v1.13.2.tar.gz 
$ cd tensorflow-1.13.2
$ ./configure

## Build transform_graph tool in tensorflow About tranform_graph https://github.com/tensorflow/tensorflow/blob/master/tensorflow/tools/graph_transforms/README.md#using-the-graph-transform-tool

$ bazel build tensorflow/tools/graph_transforms:transform_graph

## Quantize protobuf using Graph Transform Tool

$ bazel-bin/tensorflow/tools/graph_transforms/transform_graph  --in_graph=/tmp/classify_image_graph_def.pb --out_graph=/tmp/inception_v3_quantized.pb  --inputs="Mul" --outputs='softmax'  --transforms='add_default_attributes strip_unused_nodes(type=float, shape="1,299,299,3") remove_nodes(op=Identity, op=CheckNumerics) fold_constants(ignore_errors=true) fold_batch_norms fold_old_batch_norms quantize_weights quantize_nodes fold_constants strip_unused_nodes sort_by_execution_order'

##Then convert the quantized protobuf into a C file:

$ cd $HEXAGON_SDK_ROOT/examples/hexagon_nn/
$ virtualenv -p python2 env2
$ source env2/bin/activate
$ pip install -r environments/req2.txt
$ cd -
$ python  $HEXAGON_SDK_ROOT/examples/hexagon_nn/scripts/tensorflow_to_hexagon_nn.py /tmp/inception_v3_quantized.pb $HEXAGON_SDK_ROOT/examples/hexagon_nn/tutorials/007-tensorflow-to-hexagon-nn/inceptionv3_v1.yaml > iv3.c
$ cp iv3.c  $HEXAGON_SDK_ROOT/libs/hexagon_nn/2.5/

Option 2: download iv3.c file which I generated using above steps.
========
I can share my iv3.c file incase you want to avoid installing tensor flow on your machine.
$ cp iv3.c  $HEXAGON_SDK_ROOT/libs/hexagon_nn/2.5/

Step 4: Compile new fastrpc library
------------------------------------------------------

$ git clone -b  automake https://git.linaro.org/landing-teams/working/qualcomm/fastrpc.git
$ cd fastrpc
$ ./autogen.sh
$ ./configure --host=aarch64-linux-gnu
$ make
$ cp src/.libs/libadsprpc.so* $HEXAGON_SDK_ROOT/libs/common/remote/ship/UbuntuARM_Debug_aarch64/
$ cp src/.libs/libcdsprpc.so* $HEXAGON_SDK_ROOT/libs/common/remote/ship/UbuntuARM_Debug_aarch64

Step 5: Compile hexagon-nn shared userspace library.
------------------------------------------------------

## Patch Hexagon NN with patch to compile as shared library for userspace.
$ cd $HEXAGON_SDK_ROOT/libs/hexagon_nn/2.5/
$ wget http://people.linaro.org/~srinivas.kandagatla/hexagon_nn_3.4.2_shared_lib.patch
$ patch -p1 < hexagon_nn_3.4.2_shared_lib.patch
$ make tree  V=UbuntuARM_Debug_aarch64 CDSP_FLAG=1 V65=1 GRAPHINIT="iv3.c"


Step 6: Push files to target
------------------------------------------------------
$ ssh linaro@DB_IP_ADDR mkdir /home/linaro/apps /home/linaro/dsp

$ scp $HEXAGON_SDK_ROOT/output/testsig*.so linaro@DB_IP_ADDR:~/dsp/
$ scp $HEXAGON_SDK_ROOT/output/libhexagon_nn_skel.so linaro@DB_IP_ADDR:~/dsp/

$ scp $HEXAGON_SDK_ROOT/libs/hexagon_nn/2.5/UbuntuARM_Debug_aarch64/ship/libhexagon_nn.so linaro@DB_IP_ADDR:~/apps/
$ scp $FASTRPC_LIB_PATH/src/.libs/libcdsp* linaro@DB_IP_ADDR:~/apps/
$ scp $FASTRPC_LIB_PATH/src/.libs/libadsp* linaro@DB_IP_ADDR:~/apps/
$ scp $FASTRPC_LIB_PATH/src/.libs/*rpcd linaro@DB_IP_ADDR:~/apps/


######################## on TARGET DB845c ###############################

Step 1: Install new fastrpc lib on DB845c
------------------------------------------------------
## For now remove any old adsprpc libraries. new releases should not need this, but for now lets do it.
$ rm /usr/local/libadsp* /usr/local/libcdsp* /usr/local/bin/cdsprpcd /usr/local/bin/adsprpcd
$ cp /home/linaro/apps/libcdsp* /usr/local/lib/
$ cp /home/linaro/apps/libcdsp* /usr/local/lib/
$ cp /home/linaro/apps/*rpcd /usr/local/bin

Step 2: Copy dsp libraries to correct place and start cdsprpcd deamon
------------------------------------------------------
$ mkdir /dsp
$ mount /dev/disk/by-partlabel/dsp_a /dsp
$ cp /home/linaro/dsp/* /dsp/cdsp/
$ export ADSP_LIBRARY_PATH=/dsp/cdsp
$ cdsprpcd&

Step 3: Install OpenCV on the Target
------------------------------------------------------
##Its possible to optimize opencv by enabling neon but for now lets ignore this!

$ git clone https://github.com/opencv/opencv.git
$ cd ~/opencv
$ mkdir build
$ cd build
$ cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local ..
$ make -j8
$ sudo make install

Or

$ sudo apt-get install libopencv-dev python3-opencv


Step 4: Build OpenCV Imagenet application and run
------------------------------------------------------
## To Build ##
$ git clone https://git.linaro.org/people/srinivas.kandagatla/ImagenetCv.git
$ cd ImagenetCv
$ cmake .
$ make
$ ./ImagenetCv

Same instructions apply to images branch aswell.
This should show up a live video with Image Classifications.

